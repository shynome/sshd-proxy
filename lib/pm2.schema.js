const { 
  GraphQLObjectType,
  GraphQLString,
  GraphQLInt,
  GraphQLBoolean,
  GraphQLList,
  GraphQLScalarType,
} = require('graphql')
exports.Any = new GraphQLScalarType({ name: 'Any', serialize: a=>a })
exports.Date = new GraphQLScalarType({ name: 'Date', serialize: a=>a.toString() })
exports.Monit = new GraphQLObjectType({
  name: 'Monit',
  fields:{
    memory: {
      type: GraphQLInt,
      description: 'The number of bytes the process is using.'
    },
    cpu: {
      type: GraphQLInt,
      description: 'The percent of CPU being used by the process at the moment.'
    },
  }
})
exports.Pm2Env = new GraphQLObjectType({
  name: 'Pm2Env',
  description: 'The list of path variables in the process’s environment',
  fields:{
    pm_cwd: {
      type: GraphQLString,
      description: 'The working directory of the process.',
    },
    pm_out_log_path: {
      type: GraphQLString,
      description: 'The stdout log file path.',
    },
    pm_err_log_path: {
      type: GraphQLString,
      description: 'The stderr log file path.',
    },
    exec_interpreter: {
      type: GraphQLString,
      description: 'The interpreter used.',
    },
    pm_uptime: {
      type: exports.Date,
      description: 'The uptime of the process.',
    },
    unstable_restarts: {
      type: GraphQLInt,
      description: 'The number of unstable restarts the process has been through.',
    },
    restart_time: {
      type: GraphQLInt,
    },
    status: {
      type: GraphQLString,
      description: `'online' | 'stopping' | 'stopped' | 'launching' | 'errored' | 'one-launch-status'`,
    },
    instances: {
      type: GraphQLInt,
      description: 'The number of running instances.',
    },
    pm_exec_path: {
      type: GraphQLString,
      description: 'The path of the script being run in this process.',
    },
  }
})
exports.ProcessDescription = new GraphQLObjectType({
  name:'ProcessDescription',
  fields:{
    'name': {
      type: GraphQLString,
      description: 'The name given in the original start command.',
    },
    pid: {
      type: GraphQLString,
      description: 'The pid of the process.'
    },
    pm_id: {
      type: GraphQLString,
      description: 'The pid for the pm2 God daemon process.'
    },
    monit: {
      type: exports.Monit,
    },
    pm2_env: {
      type: exports.Pm2Env,
      description: 'The list of path variables in the process’s environment',
    },
  }
})
exports.Command = new GraphQLObjectType({
  name: 'Command',
  fields:{
    locked: { type: GraphQLBoolean },
    metadata: { type: exports.Any },
    started_at: { type: exports.Any },
    finished_at: { type: exports.Any },
    error: { type: exports.Any },
  }
})
exports.Proc = new GraphQLObjectType({
  name: 'Proc',
  fields:{
    name:{ type: GraphQLString },
    vizion:{ type: GraphQLBoolean },
    autorestart:{ type: GraphQLBoolean },
    exec_mode:{ type: GraphQLString },
    exec_interpreter:{ type: GraphQLString },
    pm_exec_path:{ type: GraphQLString },
    pm_cwd:{ type: GraphQLString },
    instances:{ type: GraphQLInt },
    node_args: { type: GraphQLList(GraphQLString) },
    pm_out_log_path:{ type: GraphQLString },
    pm_err_log_path:{ type: GraphQLString },
    pm_pid_path:{ type: GraphQLString },
    status:{ type: GraphQLString },
    pm_uptime:{ type: exports.Date },
    axm_actions: { type: GraphQLList(exports.Any) },
    axm_monitor: { type: exports.Any },
    axm_dynamic: { type: exports.Any },
    vizion_running:{ type: GraphQLBoolean },
    created_at:{ type: GraphQLInt },
    pm_id:{ type: GraphQLInt },
    restart_time:{ type: GraphQLInt },
    unstable_restarts:{ type: GraphQLInt },
    started_inside:{ type: GraphQLBoolean },
    command: { type: exports.Command },
    versioning: { type: exports.Any },
    exit_code:{ type: GraphQLInt },
  }
})