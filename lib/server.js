const proxy_rule = require('./proxy_rule')
const { server } = require("./app") ;
const { pm2 } = require('./pm2')
const Express = require('express')
const app = Express()

server.applyMiddleware({ app: app })

const path = require("path")
app.use(Express.static(path.join(__dirname,"../static")))

app.listen(4000,"0.0.0.0",function(){
  // @ts-ignore
  console.log(`server running at http://127.0.0.1:${this.address().port}${server.graphqlPath}`)
})

