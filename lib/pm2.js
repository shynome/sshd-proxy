const pm2 = require('pm2')
const proxy_rule = require('./proxy_rule')
const fs = require('fs')
const { apps_filepath } = require("./config")

class PM2 {
  constructor(){
    /**@type {0|1} */
    this.link_status = 0
    /**@type {any[]} */
    this.apps = require(apps_filepath).apps
    this.pm2_connect = new Promise((rl,rj)=>{
      pm2.connect(err=>{
        if(err)return rj(err);
        this.link_status = 1
        rl()
      })
    })
  }
  apps_save(){
    fs.writeFileSync(apps_filepath,JSON.stringify({ apps: this.apps },null,2))
  }
  async init(){
    if(this.link_status === 0){
      await this.pm2_connect
    }
  }
  /**@param {number} [pm_id]  */
  async list(pm_id){
    await this.init()
    /**@type {pm2.ProcessDescription[]} */
    let list = await new Promise((rl,rj)=>{
      pm2.list((err,list)=>{
        if(err)return rj(err);
        rl(list.filter(proc=>proc.name!=='sshd-proxy'))
      })
    })
    if(typeof pm_id === 'number'){
      list = list.filter(pm2_proc=>pm2_proc.pm_id===pm_id)
    }
    return list
  }
  /**@param {string} rule  */
  async add_proxy(rule){
    await this.init()
    let startOptions = proxy_rule.parse(rule)
    let result = new Promise((rl,rj)=>{
      pm2.start(startOptions,(err,proc)=>{
        if(err)return rj(err);
        // @ts-ignore
        rl(proc[0])
      })
    })
    let index = this.apps.map(({name})=>name).indexOf(rule)
    if(index === -1 ){
      this.apps.push(startOptions)
    }else{
      this.apps[index] = startOptions
    }
    this.apps_save()
    return result
  }
  /**@param {string} rule */
  async del_proxy(rule){
    await this.init()
    let result = new Promise((rl,rj)=>{
      pm2.delete(rule,(err,proc)=>{
        if(err)rj(err)
        rl(proc)
      })
    })
    this.apps = this.apps.filter(({ name })=>name!==rule)
    this.apps_save()
    return result
  } 
}

// pm2 wrapper
exports.pm2 = new PM2()
