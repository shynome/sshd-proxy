require('dotenv').config()

const path = require('path')
exports.apps_filepath = path.join(process.cwd(),"data/app.json")

exports.dev = !/production/i.test(process.env.NODE_ENV)
