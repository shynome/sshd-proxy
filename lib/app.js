const { dev } = require("./config");

const { ApolloServer, AuthenticationError } = require('apollo-server-express')

const { schema } = require("./schema");
const TOKEN = process.env.TOKEN || 'node-lover'

exports.server = new ApolloServer({
  schema: schema,
  introspection: true,
  // @ts-ignore
  context: ({req})=>{
    if(req.body.operationName === "IntrospectionQuery")return;
    let token = req.headers.token
    if(token !== TOKEN){
      throw new AuthenticationError('TOKEN 认证失败')
    }
    return { token: req.headers.token }
  },
})

