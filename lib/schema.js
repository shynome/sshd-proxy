const {
  GraphQLSchema,
  GraphQLObjectType,
  GraphQLString,
  GraphQLList,
  GraphQLNonNull,
  GraphQLInt,
  GraphQLBoolean,
} = require("graphql");
const { pm2 } = require('./pm2')
const pm2_schema = require('./pm2.schema')

const query = new GraphQLObjectType({
  name: 'query',
  fields:{
    list:{
      type: GraphQLList(pm2_schema.ProcessDescription),
      args:{
        pm_id: { type: GraphQLInt }
      },
      async resolve(rootValue,input){
        return pm2.list(input.pm_id)
      }
    },
    login:{
      type: GraphQLBoolean,
      resolve(){ return true }
    },
  }
})

const mutation = new GraphQLObjectType({
  name: 'mutation',
  fields: {
    add_proxy: {
      type: pm2_schema.Proc,
      args: {
        rule: { type: GraphQLNonNull(GraphQLString) }
      },
      async resolve(rootValue, input){
        return pm2.add_proxy(input.rule)
      }
    },
    del_proxy: {
      type: pm2_schema.Proc,
      args: {
        rule: { type: GraphQLNonNull(GraphQLString) }
      },
      async resolve(rootValue, input){
        return pm2.del_proxy(input.rule)
      }
    }
  }
})

exports.schema = new GraphQLSchema({ query, mutation, })
