const fs = require('fs')
const { apps_filepath } = require('../lib/config')

if( fs.existsSync(apps_filepath) ){
  process.exit(0)
}

/**@type {any[]} */
let apps = [
  {
    name: "sshd-proxy",
    cwd: ".",
    script: "lib/server.js"
  }
]
const proxy_rule = require("../lib/proxy_rule")
// 初始化来自环境变量中设置的规则
if(typeof process.env.PROXY_RULES === 'string'){
  apps = apps.concat(proxy_rule.formatStr(process.env.PROXY_RULES).map(proxy_rule.parse))
}

fs.writeFileSync(apps_filepath,JSON.stringify({apps},null,2))
