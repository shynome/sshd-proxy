### 用法

定义 `SSHD_PROXY` 环境变量, 类似下面:

```sh
docker run --rm -ti \
  -v ssh:/root/.ssh \
  -v datavolume:/app/data \
  -e PROXY_RULES='5001:db-host:3007,5002:db-host2:192.168.0.2:3007' \
  shynome/sshd-proxy
```

### PROXY_RULE

```sh
# [要在运行机上暴露的端口]:[ssh主机名]:[ssh主机可访问的地址.可不填,默认是127.0.0.1]:[ssh主机可访问的地址端口]
  5002                    :db-host2   :192.168.0.2                                 :3007
```


### feature

- [ ] 添加web管理界面 (遥遥无期)