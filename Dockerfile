FROM node:10-alpine

ENV NODE_ENV=production

RUN apk add --no-cache openssh 

WORKDIR /tmp/pkg
COPY pkg-deps.json /tmp/pkg/package.json
RUN npm i --registry=https://registry.npm.taobao.org

COPY npm-pack.tgz /tmp/npm-pack.tgz
RUN set -x \
  && tar -xzf /tmp/npm-pack.tgz -C /tmp \
  && mv /tmp/package /app \
  && mv /tmp/pkg/node_modules /app

RUN rm -rf /tmp/npm-pack.tgz /tmp/pkg $(npm config get cache)

WORKDIR /app

EXPOSE 5000-5200

ENV PROXY_RULES="" \
    # win10 现在自带了一个 openssh , 默认会调用这个而不是调用 git bash 里面的, 所以加个可选项
    SSH_EXEC_PATH="" \
    # 加个默认 TOKEN 吧, 免得没有任何认证
    TOKEN="node-lover"

CMD [ "npm", "start" ]
