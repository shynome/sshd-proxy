/// <reference path="./app.type.d.ts" />

const h = new Vue().$createElement
const tips = ["暴露的端口","ssh主机","ssh主机可访问的地址","ssh主机可访问的地址端口"]

/**
 * @typedef graphql_args
 * @property {string} [query]
 * @property {string} [mutation]
 * @property {Object} [variables]
 */
/**
 * 
 * @param {graphql_args} args 
 * @param {Object} headers
 * @returns { Promise<{ errors:{message:string}[], data: any }> }
 */
function graphql(args,headers={ token: App.token }){
  return fetch('/graphql', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      ...headers,
    },
    body: JSON.stringify(args)
  })
  .then(r=>r.json())
}

const RuleAdd = Vue.extend({
  data(){
    return {
      xrule: ['','','','']
    }
  },
  methods: {
    Add(){
      console.log(this.rule)
    }
  },
  computed:{
    rule(){
      return this.xrule.join(':')
    }
  },
  render(h){
    let items = ['number','text','text','number'].map((v,i)=>{
      return h('td',{},[
        h('input',{
          attrs:{ type: v, placeholder: tips[i] },
          on: {
            change: e=>{
              e.preventDefault()
              this.xrule[i] = e.target.value
            }
          }
        })
      ])
    })
    let btn = h('td',{  },[
      h('button',{ on:{ click: ()=>this.Add() } },'add')
    ])
    return h('tr',{},items.concat(btn))
  }
})

const RuleItem = Vue.extend({
  props: [ "rule" ],
  render(h){
    /**@type {string} */
    let rule = this.rule
    let xrule = rule.split(":")
    if(typeof xrule[3] === "undefined"){
      xrule[3] = xrule[2]
      xrule[2] = '127.0.0.1'
    }
    let tds = xrule.map(v=>h('td',{},v))
    tds.push(h('td',{ },[
      h('button',{ },'stop'),
      h('button',{ },'del')
    ]))
    return h('tr',{},tds)
  }
})

const Login = Vue.extend({
  props: [ 'token' ],
  data(){
    return {
      token: this.$props.token,
      lock: false
    }
  },
  created(){
    if(this.token){
      this.login()
    }
  },
  methods:{
    /**@param {Event} e */
    async login(e){
      if(this.lock)return;
      else this.lock = true;
      if(typeof e!=='undefined'){
        e.preventDefault()
      }
      let res = await graphql(
        { query: `{ login }`, },
        { token: this.token }
      )
      if(res.errors && res.errors.length){
        alert(`登录失败. 原因如下 \n\n ${res.errors.map(r=>r.message).join('\n')}`)
      }else{
        alert(`登录成功`)
        App.token = this.token
        App.loginStatus = true
        location.reload()
      }
      this.lock = false
    }
  },
  render(h){
    return h('form',{ on:{ submit: this.login } },[
      h('input',{
        attrs:{ placeholder: "token" },
        on:{
          /**@param {Event} e */
          change: e=>this.token=e.target.value
        }
      }),
      h('button',{ attrs:{ type: 'submit', disable: this.lock } },'Login')
    ])
  }
})

const thead = h('thead',{},[
  h('tr',{},tips.concat("操作").map(v=>h('th',{},v)))
])

const App = window.App = new Vue({
  el: '#app',
  data:{
    rules: ["3306:127.0.0.1:3306"],
  },
  computed: {
    loginStatus: {
      get(){
        return sessionStorage.getItem('loginStatus') === '1'
      },
      /**@param {boolean} val  */
      set(val){
        return sessionStorage.setItem('loginStatus', val?"1":"0")
      }
    },
    token: {
      get(){
        return sessionStorage.getItem('token') || ''
      },
      /**@param {string} val  */
      set(val){
        return sessionStorage.setItem('token', val)
      }
    }
  },
  methods:{
  },
  render(h){
    if(!this.loginStatus){
      return h(Login,{ props: { token: this.token } })
    }
    let items = this.rules.map(rule=>h(RuleItem,{ props:{ rule, } }))
    items = items.concat(h(RuleAdd))
    return h('table',{},[
      thead,
      h('tbody',{},items)
    ])
  }
})